#include "c_uart2.h"
#include "uart2.hpp"

// Assuming UART2 is already initialized
// You may want to also add uart2_init() in C

bool uart2_getchar(char *byte, uint32_t timeout_ms) {
    return Uart2::getInstance().getChar(byte, timeout_ms);
}
bool init(unsigned int baudRate){
   return Uart2::getInstance().init(baudRate,32,64);
}

bool putChar(char out, unsigned int timeout){
   return Uart2::getInstance().putChar(out,timeout);
}

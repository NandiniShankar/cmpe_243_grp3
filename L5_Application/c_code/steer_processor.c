/*
 * steer_processor.c
 *
 *  Created on: Feb 25, 2019
 *      Author: Anurag
 */
#include "steer_processor.h"
void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm){

    if(right_sensor_cm<50 || left_sensor_cm < 50 )
    {
        if(left_sensor_cm > right_sensor_cm){
            steer_left();
        }
        else if (right_sensor_cm > left_sensor_cm){
            steer_right();
        }
        else
        {

        }
    }

}

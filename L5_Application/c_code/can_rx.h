/*
 * can_rx.h
 *
 *  Created on: Mar 6, 2019
 *      Author: Anurag
 */

#ifndef CAN_RX_H_
#define CAN_RX_H_
#include <stdint.h>
#include <stdbool.h>
#include "can.h"
void check_can_bus_off_rx(void);
void can_app_rx(void);
void can_app_init_rx(void);
bool check_valid_msg(can_msg_t *rxmsg);

#endif /* CAN_RX_H_ */

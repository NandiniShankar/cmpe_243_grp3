/*
 * c_gpio.h
 *
 *  Created on: Mar 6, 2019
 *      Author: Anurag
 */

#ifndef C_GPIO_H_
#define C_GPIO_H_
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif
void set_as_input(void);
bool read_pin(void);
void led_on(void);
void led_off(void);
uint8_t get_light_sensor_value(void);
void display_LED(char value);
#ifdef __cplusplus
}
#endif


#endif /* C_GPIO_H_ */

/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */
#include <c_code/can_tx.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "io_led_display.h"

bool C_period_init(void) {
    can_app_init_tx();
    //can_app_init_rx();
    return true;
}
bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    check_can_bus_off_tx();
    //check_can_bus_off_rx();
}

void C_period_10Hz(uint32_t count) {
    (void) count;
    can_app_tx();
    //can_app_rx();

}

void C_period_100Hz(uint32_t count) {
    (void) count;
    //can_app_rx();

}

void C_period_1000Hz(uint32_t count) {
    (void) count;

}

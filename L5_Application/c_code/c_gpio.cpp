/*
 * c_gpio.cpp
 *
 *  Created on: Mar 6, 2019
 *      Author: Anurag
 */

#include "c_gpio.h"
#include "gpio.hpp"
#include "io.hpp"

GPIO switch_ip(P1_9);

void set_as_input(void){
    switch_ip.setAsInput();
}
bool read_pin(void){
   return switch_ip.read();
}
void led_on(void){
    LE.on(1);
}
void led_off(void){
    LE.off(1);
}

uint8_t get_light_sensor_value(void){
    return LS.getPercentValue();
}

void display_LED(char value){
    LD.setNumber(value);
}

/*
 * queue.c
 *
 *  Created on: Feb 28, 2019
 *      Author: Anurag
 */


#include"queue.h"
#include "string.h"

void queue__init(queue_S *queue){
    queue->current_capacity = 0;
    queue->front = 0;
    queue->end = 0;
    memset(&queue->queue_memory,0,100);
}

bool queue__push(queue_S *queue, uint8_t push_value){
    if(queue->current_capacity < MAX_CAPACITY){
        queue->queue_memory[queue->end++] = push_value;
        queue->end %= MAX_CAPACITY;
        queue->current_capacity++;
        return true;
    }
    else return false;
}

bool queue__pop(queue_S *queue, uint8_t *pop_value){
    if(queue->current_capacity > 0){
        *pop_value = queue->queue_memory[queue->front++];
        queue->front %= MAX_CAPACITY;
        queue->current_capacity--;
        return true;
    }
        return false;
}

size_t queue__get_count(queue_S* queue){
    return queue->current_capacity;
}


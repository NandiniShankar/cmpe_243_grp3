/*
 * can_rx.c
 *
 *  Created on: Mar 6, 2019
 *      Author: Anurag
 */

#include "can_rx.h"
#include "FreeRTOS.h"
#include "c_gpio.h"
#include "_can_dbc/generated_can.h"

void check_can_bus_off_rx(void){
    if(CAN_is_bus_off(can1))
    CAN_reset_bus(can1);
    else
        return;
}
 void can_app_init_rx(void){
     CAN_init(can1,100,6,0,NULL,NULL);
     CAN_bypass_filter_accept_all_msgs();
     CAN_reset_bus(can1);
}

 void can_app_rx(void){
      /*can_msg_t rxmsg;
      SWITCH_CMD_t sensor_msg ={0};
      if(CAN_rx(can1, &rxmsg, 0)){
          dbc_msg_hdr_t msg_hdr;
          msg_hdr.dlc = rxmsg.frame_fields.data_len;
          msg_hdr.mid = rxmsg.msg_id;
          dbc_decode_SWITCH_CMD(&sensor_msg,rxmsg.data.bytes,&msg_hdr);
          display_LED(sensor_msg.LIGHT_Sensor_Value);
          led_off();
      }
      else{
          dbc_handle_mia_SWITCH_CMD(&sensor_msg,10);
          led_on();
      }*/

  }

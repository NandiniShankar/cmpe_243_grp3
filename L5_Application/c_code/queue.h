/*
 * queue.h
 *
 *  Created on: Feb 28, 2019
 *      Author: Anurag
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#define MAX_CAPACITY 100
typedef struct{
    uint8_t queue_memory[MAX_CAPACITY];
    uint8_t current_capacity;
    uint8_t front;
    uint8_t end;
}queue_S;

void queue__init(queue_S *queue);
bool queue__push(queue_S *queue, uint8_t push_value);
bool queue__pop(queue_S * queue, uint8_t *pop_value);
size_t queue__get_count(queue_S *queue);



#endif /* QUEUE_H_ */

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../L5_Application/c_code/c_period_callbacks.c \
../L5_Application/c_code/can_rx.c \
../L5_Application/c_code/can_tx.c \
../L5_Application/c_code/queue.c \
../L5_Application/c_code/steer_processor.c 

CPP_SRCS += \
../L5_Application/c_code/c_gpio.cpp \
../L5_Application/c_code/c_uart2.cpp \
../L5_Application/c_code/c_uart3.cpp \
../L5_Application/c_code/io_led_display.cpp 

OBJS += \
./L5_Application/c_code/c_gpio.o \
./L5_Application/c_code/c_period_callbacks.o \
./L5_Application/c_code/c_uart2.o \
./L5_Application/c_code/c_uart3.o \
./L5_Application/c_code/can_rx.o \
./L5_Application/c_code/can_tx.o \
./L5_Application/c_code/io_led_display.o \
./L5_Application/c_code/queue.o \
./L5_Application/c_code/steer_processor.o 

C_DEPS += \
./L5_Application/c_code/c_period_callbacks.d \
./L5_Application/c_code/can_rx.d \
./L5_Application/c_code/can_tx.d \
./L5_Application/c_code/queue.d \
./L5_Application/c_code/steer_processor.d 

CPP_DEPS += \
./L5_Application/c_code/c_gpio.d \
./L5_Application/c_code/c_uart2.d \
./L5_Application/c_code/c_uart3.d \
./L5_Application/c_code/io_led_display.d 


# Each subdirectory must supply rules for building sources it contributes
L5_Application/c_code/%.o: ../L5_Application/c_code/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -ffunction-sections -fdata-sections -Wall -Wshadow -Wlogical-op -Wfloat-equal -DBUILD_CFG_MPU=0 -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\newlib" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L0_LowLevel" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS\include" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS\portable" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS\portable\no_mpu" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L2_Drivers" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L2_Drivers\base" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L3_Utils" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L3_Utils\tlm" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO\fat" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO\wireless" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L5_Application" -std=gnu++11 -fabi-version=0 -fno-exceptions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

L5_Application/c_code/%.o: ../L5_Application/c_code/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -ffunction-sections -fdata-sections -Wall -Wshadow -Wlogical-op -Wfloat-equal -DBUILD_CFG_MPU=0 -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\newlib" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L0_LowLevel" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS\include" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS\portable\no_mpu" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L2_Drivers" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L2_Drivers\base" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L3_Utils" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L3_Utils\tlm" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO\fat" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO\wireless" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L5_Application" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



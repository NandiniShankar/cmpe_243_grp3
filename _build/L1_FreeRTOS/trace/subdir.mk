################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../L1_FreeRTOS/trace/trcKernelPort.c \
../L1_FreeRTOS/trace/trcSnapshotRecorder.c \
../L1_FreeRTOS/trace/trcStreamingPort.c \
../L1_FreeRTOS/trace/trcStreamingRecorder.c 

OBJS += \
./L1_FreeRTOS/trace/trcKernelPort.o \
./L1_FreeRTOS/trace/trcSnapshotRecorder.o \
./L1_FreeRTOS/trace/trcStreamingPort.o \
./L1_FreeRTOS/trace/trcStreamingRecorder.o 

C_DEPS += \
./L1_FreeRTOS/trace/trcKernelPort.d \
./L1_FreeRTOS/trace/trcSnapshotRecorder.d \
./L1_FreeRTOS/trace/trcStreamingPort.d \
./L1_FreeRTOS/trace/trcStreamingRecorder.d 


# Each subdirectory must supply rules for building sources it contributes
L1_FreeRTOS/trace/%.o: ../L1_FreeRTOS/trace/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -ffunction-sections -fdata-sections -Wall -Wshadow -Wlogical-op -Wfloat-equal -DBUILD_CFG_MPU=0 -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\newlib" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L0_LowLevel" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS\include" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L1_FreeRTOS\portable\no_mpu" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L2_Drivers" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L2_Drivers\base" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L3_Utils" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L3_Utils\tlm" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO\fat" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L4_IO\wireless" -I"C:\Nandini\CMPE243\SJSU_Dev\projects\lpc1758_freertos\L5_Application" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


